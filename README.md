0. Ubuntu 18.04
1. Установить node.js:
    `curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -`

    `sudo apt-get install -y nodejs`
2. Выполнить `npm install`
3. Выполнить `npm run build`
4. В файле src/store/index.ts на строках 75 и 87 заменить https://wcapsule-api.midvikus.com/api/get_info/ на http://0.0.0.0:8080/api/get_info/
5. Выполнить команду `npm run serve`
6. В браузере Chrome открыть адрес http://localhost:8081/, нажать F12 и нажать Toggle device toolbar (Ctrl+Shift+M)

По всем вопросам Телеграм: @satoshi73