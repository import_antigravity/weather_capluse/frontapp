import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import './registerServiceWorker';
import vuetify from './plugins/vuetify';

import './scss/app.scss';

Vue.config.productionTip = false;

require('./here/mapsjs-core');
require('./here/mapsjs-service');
require('./here/mapsjs-ui');
require('./here/mapsjs-mapevents');

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
