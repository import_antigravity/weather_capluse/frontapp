import Vue from 'vue';
import Vuex from 'vuex';
import { DateTime } from 'luxon';
import axios from 'axios';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    drawer: false,
    wear: [],
    location: {
      coords: {
        longitude: 41.98375939738398,
        latitude: 45.04883040881346,
      },
    },
    places: [],
    wearimg: false,
    weather: {},
    dayTime: false,
  },
  getters: {
    getDrawer: (state) => {
      return state.drawer;
    },
    getWearImg: (state) => {
      return state.wearimg;
    },
    getDayTime: (state) => {
      return state.dayTime;
    },
    getPosition: (state) => {
      return state.location;
    },
    getPlaces: (state) => {
      return state.places;
    },
    getWeather: (state) => {
      return state.weather;
    },
    getWeat: (state) => {
      return state.wear;
    },
  },
  mutations: {
    setposition(state, pos) {
      state.location = pos;
      store.commit('loadData');
    },
    setPlaces(state, places) {
      state.places = places;
    },
    setWeather(state, weather) {
      state.weather = weather;
    },
    setWear(state, wear) {
      state.wear = wear.parts;
      state.wearimg = wear.image;
    },
    toogleDrawer(state) {
      state.drawer = !state.drawer;
    },
    setDrawer(state, value) {
      state.drawer = value;
    },
    setDayTime(state, value) {
      state.dayTime = value;
    },
    init(state) {
      navigator.geolocation.getCurrentPosition((position) => {
        store.commit('setposition', position);
      });
    },
    initData(state) {
      axios.post('https://weather-api.midvikus.com/api/get_info/', {
        lat: state.location.coords.latitude,
        lon: state.location.coords.longitude,
        date_time: DateTime.local().toISO(),
      }).then((responce) => {
        store.commit('setPlaces', responce.data.places);
        store.commit('setWeather', responce.data.weather);
        store.commit('setWear', responce.data.wear);
      });
    },
    loadData(state, daytime) {

      axios.post('https://weather-api.midvikus.com/api/get_info/', {
        lat: state.location.coords.latitude,
        lon: state.location.coords.longitude,
        date_time: DateTime.local().plus({ hours: daytime }).toISO(),
      }).then((responce) => {


        store.commit('setPlaces', responce.data.places);
        store.commit('setWeather', responce.data.weather);
        store.commit('setWear', responce.data.wear);
        // Vue.localStorage.set('weather_load', responce.data);
      }).catch((responcs) => {
        // let lsValue = Vue.localStorage.get('weather_load');
        // store.commit('setPlaces', lsValue.places);
        // store.commit('setWeather', lsValue.weather);
        // store.commit('setWear', lsValue.wear);
      });
    },
  },
});
export default store;
